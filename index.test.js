const puppeteer = require('puppeteer');
const { toMatchImageSnapshot } = require('jest-image-snapshot');

expect.extend({ toMatchImageSnapshot });
let browser = puppeteer.launch();
beforeEach(async () => {
    browser = await puppeteer.launch();
}, 100000);

afterEach(async () => {
    await browser.close();
}, 100000);

const domain = 'https://ya.ru';
const pathes = [''];
const viewports = [
    {
        name: 'desktop',
        size: {width: 1920, height: 1080}
    },
    {
        name: 'tab',
        size: {width: 768, height: 1024}
    },
    {
        name: 'mobile',
        size: {width: 375, height: 667}
    },
];
pathes.forEach(path => {
    viewports.forEach(viewport => {
        it(`Test ${viewport.name} ${path}`, async () => {
            const page = await browser.newPage();
            await page.setViewport(viewport.size);
            await page.goto(`${domain}${path}`);
            const image = await page.screenshot({fullPage: true});
        
            expect(image).toMatchImageSnapshot();
        }, 100000);
    });
});